# SkierXMLSchema

XML Schema (.xsd) for validating SkierLogs.xml

SkierLogs logs Clubs and Skiers associated with the clubs. It also keeps track of skiers entries in each season.

This project was made to solve an issue given in an Assignment. The Assignment is part of a Database course at Norwegian University of Science and Technology

## Constraints

The XML Schema sets the following constraints:

- the hierarchical structure for the SkierLogs document according to the SkierLogs.dtd file
- the value of County elements are restricted to names of actual Norwegian counties
- the value of YearOfBirth elements and Season/fallYear attributes are restricted to valid years according to the Gregorian calendar
- the value of Date elements should be valid dates
- uniqueness for attributes in Club, Skiers/Skier and Season elements (@id, @userName and @fallYear)
- the attribute @clubId in Season/Skiers should refer to the @id of a Club present in the document
- the attribute @userName in Season/Skiers/Skier should refer to the @userName of a Skier in Skiers element

## Usage

Run the .xml and the .xsd file through an online XML Schema Validator (i.e http://www.utilities-online.info/xsdvalidation), or another compatible validation tool of your choice, to validate the xml against the xsd. You can choose between SkierLogs.xml or SkierLogsShort.xml which is a shortened version of the original file, to reduce processing time.

In the InvalidTests folder you can find xml files which contains some different errors that should be rejected by the xsd.

## Authors

* **Marius Håkonsen** - *https://bitbucket.org/marhaako*
